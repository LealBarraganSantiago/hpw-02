grupos = {
  clave_grupo: '1',
  nombre_grupo: 'ISA',
  alumnos: [
    {
      numero_control: '10620064',
      nombre_alumno: 'Santiago Lopez'
    },
    {
      numero_control: '10620065',
      nombre_alumno: 'Tania Martinez'
    },
    {
      numero_control: '10620066',
      nombre_alumno: 'Santiago Leal'
    },
    {
      numero_control: '10620067',
      nombre_alumno: 'Juan Hernandez'
    },
    {
      numero_control: '10620068',
      nombre_alumno: 'Maria Mu�oz'
    },
    {
      numero_control: '10620069',
      nombre_alumno: 'Jose Jimenez'
    },
    {
      numero_control: '10620070',
      nombre_alumno: 'Pablo Cruz'
    }
  ],
  materias: [
    {
      clave_materia: '1234',
      nombre_materia: 'quimica 1'
    },
    {
      clave_materia: '1235',
      nombre_materia: 'fisica'
    },
    {
      clave_materia: '1236',
      nombre_materia: 'historia'
    },
    {
      clave_materia: '1237',
      nombre_materia: 'etica'
    },
    {
      clave_materia: '1238',
      nombre_materia: 'matematicas'
    },
    {
      clave_materia: '1239',
      nombre_materia: 'biologia'
    },
    {
      clave_materia: '1240',
      nombre_materia: 'ingles'
    }
  ],
  profesor: [
    {
      clave_profesor: '123456',
      nombre_profesor: 'Marcos Hernandez'
    }
  ],
  horarios: [
    {
      clave_horario: '10',
      hora_inicial: '09:00',
      hora_final: '10:00',
      clave_materia: '1234'
    },
    {
      clave_horario: '11',
      hora_inicial: '10:00',
      hora_final: '11:00',
      clave_materia: '1235'
    },
    {
      clave_horario: '12',
      hora_inicial: '11:00',
      hora_final: '12:00',
      clave_materia: '1236'
    },
    {
      clave_horario: '13',
      hora_inicial: '12:00',
      hora_final: '13:00',
      clave_materia: '1237'
    },
    {
      clave_horario: '14',
      hora_inicial: '13:00',
      hora_final: '14:00',
      clave_materia: '1238'
    },
    {
      clave_horario: '15',
      hora_inicial: '14:00',
      hora_final: '15:00',
      clave_materia: '1239'
    },
    {
      clave_horario: '16',
      hora_inicial: '15:00',
      hora_final: '16:00',
      clave_materia: '1240'
    }
  ]
}
//daado un grupo imprimir una lista de alumnos

function alumnos(clave_grupo)
{
  if (grupos['clave_grupo'] == clave_grupo)
  {
    for (i = 0; i < grupos['alumnos'].length; i++)
    {
      numero_control = (grupos['alumnos'][i]['numero_control']);
      nombre = (grupos['alumnos'][i]['nombre_alumno']);
      console.log([i + 1] + '|' + numero_control + '|' + nombre);
    }
  } 
  else
  {
    return 'no existe el grupo';
  }
}


console.log(alumnos('1'));




//dado un gripo
function horario_profesor(clave_grupo) 
{
  if (grupos['clave_grupo'] == clave_grupo)
  {
    for (i = 0; i < grupos['horarios'].length; i++) 
     {
       materia =(grupos['materias'][i]['clave_materia']);
       materia2=(grupos['horarios'][i]['clave_materia']);
       nombre_materia=(grupos['materias'][i]['nombre_materia']);
       profesor = (grupos['profesor'][0]['nombre_profesor']);
       a = (grupos['horarios'][i]['hora_inicial']) +
                    '-' + (grupos['horarios'][i]['hora_final']) + '|' + profesor+'|'+nombre_materia;
      
          if(materia===materia2)
            {
              console.log(a);
            }
         else
         {
         return " ";}
     }
  } 
  else
    {
      return 'no existe el grupo';
    }
}


console.log(horario_profesor('1'));


//dentro de un grupo buscar a un alumno,profesor o materia

//1. buscar alumno

function busca_alumno(numero_control)
{
   
  for(i=0;i<grupos['alumnos'].length;i++)
  {
    alumno=[ (grupos['alumnos'][i]['numero_control']) ];
     //  console.log(alumno)

    for(j=0;j<alumno.length;j++)
    {
      if(alumno[j]===numero_control)
        {
          return ( "alumno registrado");
        }
    
    }
   }
  return  ("alumno no registrado");
}

console.log(busca_alumno('10620064'));
console.log(busca_alumno('10620065'));
console.log(busca_alumno('10620090'));

//2. busca profesor

function busca_profesor(clave_profesor)
{
   
  for(i=0;i<grupos['profesor'].length;i++)
  {
    profesor=[ (grupos['profesor'][i]['clave_profesor']) ];
     //  console.log(profesor)

    for(j=0;j<profesor.length;j++)
    {
      if(profesor[j]===clave_profesor)
        {
          return ( "profesor registrado");
        }
    
    }
   }
  return  ("profesor no registrado");
}

console.log(busca_profesor('123456')); 
console.log(busca_profesor('123457')); 


//3.busca materia

function busca_materia(nombre_materia)
{   
  for(i=0;i<grupos['materias'].length;i++)
  {
    materia=[ (grupos['materias'][i]['nombre_materia']) ];
     //  console.log(profesor)
    for(j=0;j<materia.length;j++)
    {
      if(materia[j]===nombre_materia)
        {
          return ( "materia registrado");
        }
     }
   }
  return  ("materia no registrado");
}

console.log(busca_materia('quimica 1'));
console.log(busca_materia('ingles'));
console.log(busca_materia('xxxx'));
 

  // DADO UN GRUPO INSERTAR UN NUEVO ALUMNO

function agrega_alumno(clave_grupo,clave_alumno,nombre_alumno)
{
 clave=grupos['clave_grupo'];
  if(clave_grupo===clave)
  {
    for(i=0;i<grupos['alumnos'].length;i++)
    {
      num_control_alumno=[ (grupos['alumnos'][i]['numero_control']) ];
     //  console.log(profesor)
        for(j=0;j<num_control_alumno.length;j++)
        {
          if(num_control_alumno[j]===clave_alumno)
          {
            console.log ( "El alumno ya existe");
          }
          if(num_control_alumno[j]!==clave_alumno)
          {
            grupos['alumnos'] .push(clave_alumno);
            grupos['alumnos'] .push(nombre_alumno);
            console.log( "alumno registrado con exito");
            return ":)"
          }
        }
     }
 
   }
      return "el grupo no existe";

}

console.log(agrega_alumno('1','10620064','Santiago Leal Barragan'));

console.log(agrega_alumno('1','10620080','antonio Lopez'));

 

